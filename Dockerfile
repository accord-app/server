FROM node:11-alpine

WORKDIR /src/server
ENV NODE_ENV=production PORT=80

COPY package.json yarn.lock ./
RUN apk add --no-cache --virtual build-deps python make g++ && \
    yarn install && \
    apk del build-deps

COPY . ./

EXPOSE 80

CMD npm start

const path = require('path');
const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const socketio = require('socket.io');

require('dotenv').config({ path: path.join(__dirname, '.env') });

const routes = require('./routes');

const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost:27017/discord-clone';

mongoose.connect(MONGODB_URI, {
  useNewUrlParser: true,
  autoReconnect: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 1000,
});
//  .catch((err) => {
//    console.error(err);
//    process.exit(1);
//  });

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(routes);

const server = app.listen(process.env.PORT || 3000);

app.io = socketio(server);

// Initialize WebSocket namespaces for client subscriptions.
['/guilds', '/channels', '/messages'].forEach((nsp) => {
  app.io.of(nsp);
});

const express = require('express');
const asyncHandler = require('express-async-handler');

const Channel = require('../models/channel');
const Guild = require('../models/guild');

const router = express.Router({ mergeParams: true });

// TODO: Verify permissions owner?

router.get('/', asyncHandler(async (req, res) => {
  const { guildId } = req.params;

  const channels = await Channel.find({ guild: guildId });

  res.json(channels);
}));

router.post('/', asyncHandler(async (req, res) => {
  const { name } = req.body;
  const { guildId } = req.params;

  const [channel, guild] = await Promise.all([
    Channel.create({ name, guild: guildId }),
    Guild.findById(guildId),
  ]);

  guild.channels.push(channel._id);


  await guild.save();

  res.json(channel);
  req.app.io.of('/channels').emit('new', channel);
}));

router.patch('/:id', asyncHandler(async (req, res) => {
  const { name } = req.body;

  const channel = await Channel.findByIdAndUpdate(req.params.id, { name });

  res.send(channel.toJSON());
}));

module.exports = router;

const express = require('express');
const asyncHandler = require('express-async-handler');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

const router = express.Router();

const sendToken = (user, res) => {
  res.json({ token: jwt.sign({ _id: user._id }, process.env.SECRET) });
};

router.post('/login', asyncHandler(async (req, res) => {
  const { username, password } = req.body;
  const user = await User.findOne({ username });

  if (!user) {
    res.status(400).json({ username: 'Does not exist in database' });
  } else if (await user.comparePassword(password)) {
    sendToken(user, res);
  } else {
    res.status(400).json({ password: 'Invalid' });
  }
}));

router.post('/register', asyncHandler(async (req, res) => {
  const { username, password } = req.body;

  const user = await User.create({ username, password });

  sendToken(user, res);
}));

module.exports = router;

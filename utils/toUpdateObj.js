const pick = require('lodash.pick');

module.exports = function toUpdateObject() {
  return pick(this.toObject(), this.modifiedPaths());
};
